<?php

session_start();

// Set up request and session vars
if(isset($_POST['username'])) {
  $_SESSION['username'] = $_POST['username'];
}
if(isset($_POST['trackNum'])) {
  $_SESSION['trackNum'] = $_POST['trackNum'];
}
if(isset($_POST['timePeriod'])) {
  $_SESSION['timePeriod'] = $_POST['timePeriod'];
}
if(isset($_POST['service'])) {
  $_SESSION['service'] = $_POST['service'];
}

// Generate playlist title
$playlist_title = $_SESSION['username'] . " most played ";
switch($_SESSION['timePeriod']) {
  case "7days":
    $now = date("d/m");
    $tma_ts = strtotime("7 days ago");
    $tma = date("d/m", $tma_ts);
    $playlist_title .= $tma . "-" . $now . " " . date("Y");
    break;
  case "3months";
    $now = date("M");
    $tma_ts = strtotime("3 months ago");
    $tma = date("M", $tma_ts);
    $playlist_title .= $tma . "-" . $now . " " . date("Y");
    break;
  case "6months";
    $now = date("M");
    $tma_ts = strtotime("6 months ago");
    $tma = date("M", $tma_ts);
    $playlist_title .= $tma . "-" . $now . " " . date("Y");
    break;
  case "1year":
    $now = date("M");
    $tma_ts = strtotime("1 year ago");
    $tma = date("Y", $tma_ts);
    $playlist_title .= $now . " " . $tma . "-" . $now . " " . date("Y");
    break;
  case "forever":
    $playlist_title .= "of all time";
    break;
  default:
    die("Error processing form");
    break;
}
$_SESSION['title'] = $playlist_title;

// Service specific processing
switch($_SESSION['service']) {
  case "rdio":
    include("services/rdio/process.php");
  default:
    die("Error processing form");
    break;
}