<?php

require_once 'rdio-consumer-credentials.php';
require_once 'rdio.php';

$rdio = new Rdio(array(RDIO_CONSUMER_KEY, RDIO_CONSUMER_SECRET));



$current_url = "http" . ((!empty($_SERVER['HTTPS'])) ? "s" : "") .
  "://" . $_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];

if ($_GET['logout']) {
  session_destroy();
  header('Location: '.$current_url);
}

if ($_SESSION['oauth_token'] && $_SESSION['oauth_token_secret']) {
  $rdio->token = array($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

  if ($_GET['oauth_verifier']) {
    $rdio->complete_authentication($_GET['oauth_verifier']);
    $_SESSION['oauth_token'] = $rdio->token[0];
    $_SESSION['oauth_token_secret'] = $rdio->token[1];
  }

  $currentUser = $rdio->call('currentUser');

  if ($currentUser) {
    $i = 0;
    $keys = array();

    $url = "http://ws.audioscrobbler.com/2.0/user/";
    $url .= $_SESSION['username'];
    $url .= "/toptracks.xml?period=" . $_SESSION['timePeriod'];

    $mysongs = simplexml_load_file($url);
    if(!$mysongs) {
      die("There was an error retrieving data from last.fm. Please use the back button and double check that you entered the correct username.");
    }

    foreach($mysongs->track as $track) {
      $i++;
      if($i < $_SESSION['trackNum']) {
        $query = (string)$track->name . ' ' . (string)$track->artist->name;
        $search_results = $rdio->call('search', array(
          'query' => $query,
          'types' => "Track",
          'count' => 1
        ));
        array_push($keys, $search_results->result->results[0]->key);
      }
    }

    

    $playlist = $rdio->call('createPlaylist', array(
      'name' => $_SESSION['title'],
      'description' => "Generated from last.fm",
      'tracks' => implode(",", $keys)
    ));

    header('Location: http://www.rdio.com' . $playlist->result->url);
  } else {
    session_destroy();
    header('Location: '.$current_url);
  }
} else {
  $authorize_url = $rdio->begin_authentication($current_url);
  $_SESSION['oauth_token'] = $rdio->token[0];
  $_SESSION['oauth_token_secret'] = $rdio->token[1];

  header('Location: '.$authorize_url);
}